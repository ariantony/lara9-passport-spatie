<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\Authorization\RolesController;
use App\Http\Controllers\Authorization\PermissionsController;
use App\Http\Controllers\Authorization\RolesAndPermsController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login',[UserController::class,'login']);
Route::post('register',[UserController::class,'register']);


Route::group(['middleware' => 'auth:api'], function(){
    Route::get('user-info',[UserController::class,'details']);

    Route::get('roles',[RolesController::class,'index']);
    Route::post('roles',[RolesController::class,'store']);
    Route::put('roles/{id}',[RolesController::class,'update']);
    Route::delete('roles/{id}',[RolesController::class,'destroy']);
    Route::get('roles/search', [RolesController::class,'search']);

    Route::get('permissions',[PermissionsController::class,'index']);
    Route::post('permissions',[PermissionsController::class,'store']);
    Route::put('permissions/{id}',[PermissionsController::class,'update']);
    Route::delete('permissions/{id}',[PermissionsController::class,'destroy']);
    Route::get('permissions/search', [PermissionsController::class,'search']);

    Route::post('uhr', [RolesAndPermsController::class,'uhr']);
    Route::post('uhp', [RolesAndPermsController::class,'uhp']);
    Route::post('rhp', [RolesAndPermsController::class,'rhp']);

});
