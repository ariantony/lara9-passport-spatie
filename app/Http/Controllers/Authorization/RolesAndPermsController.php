<?php

namespace App\Http\Controllers\Authorization;

use App\Models\User;
use Illuminate\Http\Request;
use App\Models\ModelHasRoles;
use App\Models\RoleHasPermission;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Controller;
use App\Http\Helper\ResponseBuilderList;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class RolesAndPermsController extends Controller
{
    // public function __construct()
    // {
    //     //$this->middleware(['role:superadmin|admin']);
    //     // $this->middleware(['permission:roles-read'])->only(['index','show','search']);
    //     // $this->middleware(['permission:roles-create'])->only(['store']);
    //     // $this->middleware(['permission:roles-update'])->only(['update']);
    //     // $this->middleware(['permission:roles-delete'])->only(['destroy']);
    // }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = RoleHasPermission::from('role_has_permissions as r')
                ->selectRaw('r.permission_id, p.name as permission, r.role_id, rs.name as role')
                ->join('roles as rs','rs.id','=','r.role_id')
                ->join('permissions as p','p.id','=','r.permission_id')
                ->orderBy('rs.id', 'DESC')
                ->paginate(15);

        $status = true;
        $message  = "Data  ditemukan.";
        $response_code = Response::HTTP_FOUND;
        $count = count($data);

        return ResponseBuilderList::result($status, $message, $data, $count, $response_code);

    }

    public function userrolehasperm($id) {

        $user = User::find($id);
        //$user->getPermissionsViaRoles();
        //$user->getDirectPermissions();
        $user->getAllPermissions();

        $status = true;
        $message  = "Data  ditemukan.";
        $response_code = Response::HTTP_OK;

        return response()->json(["status" => $status,"message" => $message,"data" => $user,], $response_code);

    }

    public function rolehasperm(Request $request, $id) {

        //dd($request->all());

        $validator = Validator::make($request->all(),[
            'permission_id' => ['required','min:1']
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(),Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $role = Role::find($id);

        if (!empty($request->permission_id)) {
            foreach ($request->permission_id as $key => $value) {
                $insert = $role->givePermissionTo($value);
            }
        } else {
            $permissions = '';
        }

        $response = [
            'message'=>'Data successfully inserted.',
            'data'=> $insert
        ];

        return response()->json($response, Response::HTTP_CREATED);

        //$role->syncPermissions($permissions);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     public function userhasrole(Request $request, $id) {

        $validator = Validator::make($request->all(),[
            'role_id' => ['required','min:1']
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(),Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $user = User::find($id);

        if (!empty($request->role_id)) {
            foreach ($request->role_id as $key => $value) {
                $insert = $user->assignRole($value);
            }
        } else {
            $permissions = '';
        }

        $response = [
            'message'=>'Data successfully inserted.',
            'data'=> $insert
        ];

        return response()->json($response, Response::HTTP_CREATED);

     }

     public function userhasperm (Request $request, $id) {

        $validator = Validator::make($request->all(),[
            'permission_id' => ['required','min:1']
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(),Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $user = User::find($id);

        if (!empty($request->permission_id)) {
            foreach ($request->permission_id as $key => $value) {
                //$insert = $user->assignRole($value);
                //$insert = $user->syncPermissions($value);
                $insert = $user->givePermissionTo($value);
            }
        } else {
            $permissions = '';
        }

        $response = [
            'message'=>'Data successfully inserted.',
            'data'=> $insert
        ];

        return response()->json($response, Response::HTTP_CREATED);

     }

     public  function rhp(Request $request) {
         if($request->do === 'assign') {

                //$role = Role::find($role);

                $validator = Validator::make($request->all(),[
                    'permission_id' => ['required','min:1'],
                    'role' => ['required','min:1','exists:roles,id']
                ]);

                if($validator->fails()){
                    return response()->json($validator->errors(),Response::HTTP_UNPROCESSABLE_ENTITY);
                }

                $role = Role::find($request->role);

                if (!empty($request->permission_id)) {
                    foreach ($request->permission_id as $key => $value) {
                        $insert = $role->givePermissionTo($value);
                    }
                } else {
                    $permissions = '';
                }

                $response = [
                    'message'=>'Data successfully insert.',
                    'data'=> $insert
                ];

                return response()->json($response, Response::HTTP_CREATED);

         } elseif ($request->do === 'revoke') {

                $validator = Validator::make($request->all(),[
                    //'permission_id' => ['required','min:1','exists:role_has_permissions,permission_id'],
                    'role' => ['required','min:1','min:1','exists:role_has_permissions,role_id']
                ]);

                if($validator->fails()){
                    return response()->json($validator->errors(),Response::HTTP_UNPROCESSABLE_ENTITY);
                }

                $role = Role::find($request->role);

                if (!empty($request->permission_id)) {
                    foreach ($request->permission_id as $key => $value) {
                        $insert = $role->revokePermissionTo($value);
                    }
                } else {
                    $permissions = '';
                }

                $response = [
                    'message'=>'Data successfully revoked.'
                ];

                return response()->json($response, Response::HTTP_CREATED);

         } elseif ($request->do === 'sync') {

                $validator = Validator::make($request->all(),[
                    'role' => ['required','min:1','min:1','exists:role_has_permissions,role_id']
                ]);

                if($validator->fails()){
                    return response()->json($validator->errors(),Response::HTTP_UNPROCESSABLE_ENTITY);
                }

                $role = Role::find($request->role);

                if (!empty($request->permission_id)) {
                    $insert = $role->syncPermissions($request->permission_id);

                    $response = [
                        'message'=>'Data successfully synced.'
                    ];

                    return response()->json($response, Response::HTTP_CREATED);
                }

                $response = [
                    'message'=>'Data not successfully synced.'
                ];

                return response()->json($response, Response::HTTP_CREATED);

         } else {

            return 'Tidak ada aktivitas yang ada pilih.';

         }

     }

     public  function uhr(Request $request) {
        if($request->do === 'assign') {

               //$role = Role::find($role);

               $validator = Validator::make($request->all(),[
                   'role_id' => ['required','min:1'],
                   'user' => ['required','min:1','exists:users,id']
               ]);

               if($validator->fails()){
                   return response()->json($validator->errors(),Response::HTTP_UNPROCESSABLE_ENTITY);
               }

               $user = User::find($request->user);

               if (!empty($request->role_id)) {
                   foreach ($request->role_id as $key => $value) {
                       $insert = $user->assignRole($value);
                   }
               } else {
                   $role = '';
               }

               $response = [
                   'message'=>'Data successfully insert.',
                   'data'=> $insert
               ];

               return response()->json($response, Response::HTTP_CREATED);

        } elseif ($request->do === 'revoke') {

               $validator = Validator::make($request->all(),[
                   //'permission_id' => ['required','min:1','exists:role_has_permissions,permission_id'],
                   'role_id' => ['required','min:1','exists:model_has_roles,role_id'],
                   'user' => ['required','min:1','exists:model_has_roles,model_id']
               ]);

               if($validator->fails()){
                   return response()->json($validator->errors(),Response::HTTP_UNPROCESSABLE_ENTITY);
               }

               if (!empty($request->role_id)) {
                   foreach ($request->role_id as $key => $value) {
                       $delete = ModelHasRoles::where('role_id', '=', $value)
                           ->where('model_id','=', $request->user)
                           ->delete();
                   }
               } else {
                   $role = '';
               }

               $response = [
                   'message'=>'Data successfully deleted.'
               ];

               return response()->json($response, Response::HTTP_OK);

        } elseif ($request->do === 'sync') {

               $validator = Validator::make($request->all(),[
                'role_id' => ['required','min:1','exists:roles,id'],
                'user' => ['required','min:1','exists:users,id']
               ]);

               if($validator->fails()){
                   return response()->json($validator->errors(),Response::HTTP_UNPROCESSABLE_ENTITY);
               }

               $user = User::find($request->user);

               if (!empty($request->role_id)) {

                   //$insert = $role->syncPermissions($request->permission_id);

                   $insert = $user->syncRoles($request->role_id);

                   $response = [
                       'message'=>'Data successfully synced.'
                   ];

                   return response()->json($response, Response::HTTP_OK);
               }

               $response = [
                   'message'=>'Data not successfully synced.'
               ];

               return response()->json($response, Response::HTTP_NO_CONTENT);

        } else {

           return 'Tidak ada aktivitas yang ada pilih.';

        }

    }

    public  function uhp(Request $request) {
        if($request->do === 'assign') {

               //$role = Role::find($role);

               $validator = Validator::make($request->all(),[
                   'permission_id' => ['required','min:1','exists:permissions,id'],
                   'user' => ['required','min:1','exists:users,id']
               ]);

               if($validator->fails()){
                   return response()->json($validator->errors(),Response::HTTP_UNPROCESSABLE_ENTITY);
               }

               $user = User::find($request->user);

               if (!empty($request->permission_id)) {
                   foreach ($request->permission_id as $key => $value) {
                       $insert = $user->givePermissionTo($value);
                   }
               } else {
                   $permissions = '';
               }

               $response = [
                   'message'=>'Data successfully insert.',
                   'data'=> $insert
               ];

               return response()->json($response, Response::HTTP_CREATED);

        } elseif ($request->do === 'revoke') {

               $validator = Validator::make($request->all(),[
                   //'permission_id' => ['required','min:1','exists:role_has_permissions,permission_id'],
                   'permission_id' => ['required','min:1','exists:model_has_permissions,permission_id'],
                   'user' => ['required','min:1','exists:model_has_permissions,model_id']
               ]);

               if($validator->fails()){
                   return response()->json($validator->errors(),Response::HTTP_UNPROCESSABLE_ENTITY);
               }


               $user = User::find($request->user);

               if (!empty($request->permission_id)) {
                   foreach ($request->permission_id as $key => $value) {

                       //$insert = $user->givePermissionTo($value);
                       $insert = $user->revokePermissionTo($value);
                   }
               } else {
                   $permissions = '';
               }

               $response = [
                   'message'=>'Data successfully deleted.'
               ];

               return response()->json($response, Response::HTTP_OK);

        } elseif ($request->do === 'sync') {

            $validator = Validator::make($request->all(),[
                //'permission_id' => ['required','min:1','exists:role_has_permissions,permission_id'],
                'permission_id' => ['required','min:1','exists:permissions,id'],
                'user' => ['required','min:1','exists:users,id']
            ]);

            if($validator->fails()){
                return response()->json($validator->errors(),Response::HTTP_UNPROCESSABLE_ENTITY);
            }

            $user = User::find($request->user);

                    if (!empty($request->permission_id)) {

                        $insert = $user->syncPermissions($request->permission_id);

                    } else {
                        $permissions = '';
                    }

            $response = [
                'message'=>'Data successfully deleted.'
            ];

            return response()->json($response, Response::HTTP_OK);

        } else {

           return 'Tidak ada aktivitas yang ada pilih.';

        }

    }

}
