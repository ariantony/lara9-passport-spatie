<?php

namespace App\Http\Controllers\Authorization;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Helper\ResponseBuilder;
use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Database\QueryException;
use App\Http\Helper\ResponseBuilderList;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class PermissionsController extends Controller
{
    // public function __construct()
    // {
    //     //$this->middleware(['role:superadmin|admin']);
    //     // $this->middleware(['permission:roles-read'])->only(['index','show','search']);
    //     // $this->middleware(['permission:roles-create'])->only(['store']);
    //     // $this->middleware(['permission:roles-update'])->only(['update']);
    //     // $this->middleware(['permission:roles-delete'])->only(['destroy']);
    // }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Permission::all();

        //$data = Permission::orderBy('id', 'DESC')->all();

        $status = true;
        $message  = "Data  ditemukan.";
        $response_code = Response::HTTP_FOUND;
        $count = count($data);

        return ResponseBuilderList::result($status, $message, $data, $count, $response_code);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(),[
            'permissions' => ['required','min:2','unique:permissions,name']
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(),Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        if (!empty($request->permissions)) {
            foreach ($request->permissions as $key => $value) {
                $insert = Permission::create(['name' => $value ]);
            }
        } else {
            $permissions = '';
        }

        $response = [
            'message'=>'Data successfully inserted.',
            'data'=> $insert
        ];

        return response()->json($response, Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $status = true;
        $message  = "Data berhasil di ambil";
        $response_code = Response::HTTP_OK;
        $data = Permission::find($id);

        try {
            if (empty($data)){
                    $message  = "ID tidak ditemukan";
                    return ResponseBuilder::result('False', $message, '[]', '404');
                }
            return ResponseBuilder::result($status, $message, $data, $response_code);

        } catch (QueryException $e) {
            return response()->json([
                'message' => "Failed" . $e->errorInfo
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $status = true;
        $message  = "Data berhasil di ambil";
        $response_code = Response::HTTP_CREATED;
        $data = Permission::find($id);

        try {
            if (empty($data)){
                    $message  = "ID tidak ditemukan";
                    return ResponseBuilder::result('False', $message, '[]', '404');
            }

            $data = [];
            $data['name'] = $request->name;
            $data['guard_name'] = $request->guard_name;
            $data['created_at'] = now();
            $data['updated_at'] = now();


            $update = Permission::where('id','=', $id)
                    ->update($data);

            $response = [
                'message'=>'Data successfully update.',
                'status'=> $update,
                'data' => $data
            ];


            return ResponseBuilder::result($status, $message, $data, $response_code);


        } catch (QueryException $e) {
            return response()->json([
                'message' => "Failed" . $e->errorInfo
            ]);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $status = true;
        $message  = "Data berhasil di hapus";
        $response_code = Response::HTTP_OK;
        $data = Permission::find($id);

        try {
            if (empty($data)){
                    $message  = "ID tidak ditemukan";
                    return ResponseBuilder::result('False', $message, '[]', '404');
                }

            $data->delete();

            return ResponseBuilder::result($status, $message, $data, $response_code);

        } catch (QueryException $e) {
            return response()->json([
                'message' => "Failed" . $e->errorInfo
            ]);
        }
    }

    public function search() {

        $status = true;
        $message  = "Data berhasil di ambil";
        $response_code = Response::HTTP_OK;

        $data = QueryBuilder::for(Permission::class)
        ->allowedFilters(['name'])
        ->get();

        $count = count($data);

        if (empty($data)){
            $message  = "Data kosong";
            return ResponseBuilder::result('False', $message, '[]', '404');
        }

        return ResponseBuilderList::result($status, $message, $data, $count, $response_code);

    }
}
