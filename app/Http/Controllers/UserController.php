<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public $successStatus = 200;

    public function login(){
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){

            $user = Auth::user();

            $token = $user->createToken('nApp')->accessToken;
            $privileges = $user->getAllPermissions();

            return response()->json([
                'id'         => $user->id,
                'name'         => $user->name,
                'email'         => $user->email,
                'token'         => $token,
                'privileges' => $privileges
            ],$this->successStatus);
        }
        else{
            return response()->json(['error'=>'Username atau Password anda salah.'], 401);
        }
    }


    public function register(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email','unique:users,email',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $info['success'] = true ;
        $info['profile']['token'] =  $user->createToken('nApp')->accessToken;
        $info['profile']['id'] =  $user->id;
        $info['profile']['name'] =  $user->name;
        $info['profile']['email'] =  $user->email;

        //$user->assignRole('writer');

        return response()->json(['info'=>$info], $this->successStatus);
    }

    public function details()
    {
        $user = Auth::user();
        $token = $user->createToken('nApp')->accessToken;
        $privileges = $user->getAllPermissions();
        $privileges_by_roles = $user->getPermissionsViaRoles();

        return response()->json([
            'id'         => $user->id,
            'name'         => $user->name,
            'email'         => $user->email,
            'token'         => $token,
            'privileges' => $privileges,
            'privileges_by_roles' => $privileges_by_roles
        ],$this->successStatus);
    }

}
